# DriveAway

DriveAway is a **responsive web application** developed to help driving schools manage students and instructures. The main requirements of the application consists of management and basic operations of a driving school, it’s students and driving instructors.

This project was developed for a class for the **University of Aveiro**

![Calender](./images/admin.png)



## Core Tecnologies

* HTML
* CSS
* Bootstrap
* JavaScript
* JQuery
* PHP
* JSON
* AJAX
* PostgreSQL



## Main Functionality

* [x] CRUD Students
* [x] CRUD Lessons
* [x] CRUD Instructors
* [x] CRUD Workers
* [x] Calender
* [x] Student Ask Lesson
* [x] etc..


## Getting Started

1. `git clone ...`
2. Restore **dump_final.sql** to postgresql
3. Copy project folder to web server
4. Change **config.php**
5. Enjoy! 

___

## Other fotos

![Admin insert](./images/admin_insert.png)

![Ask Lesson](./images/add_lesson.png)

___

## Developed By
* Diogo Santos
* Daniel Pereira
* Gabriel Oliveira
* Jorge Godinho
* Pedro Quinta